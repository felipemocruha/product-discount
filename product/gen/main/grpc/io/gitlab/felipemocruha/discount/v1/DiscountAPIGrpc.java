package io.gitlab.felipemocruha.discount.v1;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.0.0)",
    comments = "Source: discount/v1/discount.proto")
public class DiscountAPIGrpc {

  private DiscountAPIGrpc() {}

  public static final String SERVICE_NAME = "discount.v1.DiscountAPI";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest,
      io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse> METHOD_CALCULATE_DISCOUNT =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "discount.v1.DiscountAPI", "CalculateDiscount"),
          io.grpc.protobuf.ProtoUtils.marshaller(io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DiscountAPIStub newStub(io.grpc.Channel channel) {
    return new DiscountAPIStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DiscountAPIBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DiscountAPIBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static DiscountAPIFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DiscountAPIFutureStub(channel);
  }

  /**
   */
  public static abstract class DiscountAPIImplBase implements io.grpc.BindableService {

    /**
     */
    public void calculateDiscount(io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest request,
        io.grpc.stub.StreamObserver<io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_CALCULATE_DISCOUNT, responseObserver);
    }

    @java.lang.Override public io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_CALCULATE_DISCOUNT,
            asyncUnaryCall(
              new MethodHandlers<
                io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest,
                io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse>(
                  this, METHODID_CALCULATE_DISCOUNT)))
          .build();
    }
  }

  /**
   */
  public static final class DiscountAPIStub extends io.grpc.stub.AbstractStub<DiscountAPIStub> {
    private DiscountAPIStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DiscountAPIStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountAPIStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DiscountAPIStub(channel, callOptions);
    }

    /**
     */
    public void calculateDiscount(io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest request,
        io.grpc.stub.StreamObserver<io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_CALCULATE_DISCOUNT, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DiscountAPIBlockingStub extends io.grpc.stub.AbstractStub<DiscountAPIBlockingStub> {
    private DiscountAPIBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DiscountAPIBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountAPIBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DiscountAPIBlockingStub(channel, callOptions);
    }

    /**
     */
    public io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse calculateDiscount(io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_CALCULATE_DISCOUNT, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DiscountAPIFutureStub extends io.grpc.stub.AbstractStub<DiscountAPIFutureStub> {
    private DiscountAPIFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DiscountAPIFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountAPIFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DiscountAPIFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse> calculateDiscount(
        io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_CALCULATE_DISCOUNT, getCallOptions()), request);
    }
  }

  private static final int METHODID_CALCULATE_DISCOUNT = 0;

  private static class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DiscountAPIImplBase serviceImpl;
    private final int methodId;

    public MethodHandlers(DiscountAPIImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CALCULATE_DISCOUNT:
          serviceImpl.calculateDiscount((io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountRequest) request,
              (io.grpc.stub.StreamObserver<io.gitlab.felipemocruha.discount.v1.Discount.CalculateDiscountResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    return new io.grpc.ServiceDescriptor(SERVICE_NAME,
        METHOD_CALCULATE_DISCOUNT);
  }

}
