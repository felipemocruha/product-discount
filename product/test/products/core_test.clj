(ns products.core-test
  (:require [midje.sweet :refer :all]
            [products.core :as core]))

(fact "test format-result"
  (fact "given a product and a discount, it should return a formatted map"
    (let [product {:product/id (java.util.UUID/randomUUID)
                   :product/price_in_cents 19999
                   :product/title "test"
                   :product/description "test"}
          discount {:percentage 10.0
                    :value_in_cents 18999}
          expected {:id (str (:product/id product))
                    :price_in_cents (:product/price_in_cents product)
                    :title (:product/title product)
                    :description (:product/description product)
                    :discount discount}]
      (core/format-result product discount) => expected)))
