(defproject product "0.1.0"
  :description "A service to list products."
  :license "MIT"
  :url "https://gitlab.com/felipemocruha/myproject"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [io.pedestal/pedestal.service "0.5.8"]
                 [io.pedestal/pedestal.jetty "0.5.8"]
                 [metosin/reitit "0.5.10"]
                 [metosin/reitit-pedestal "0.5.10"]
                 [metosin/jsonista "0.2.7"]                 
                 [seancorfield/next.jdbc "1.1.613"]
                 [com.zaxxer/HikariCP "3.4.2"]
                 [org.postgresql/postgresql "42.2.10"]
                 [com.cognitect/anomalies "0.1.12"]
                 [com.fzakaria/slf4j-timbre "0.3.20"]
                 [com.taoensso/timbre "4.10.0"]
                 [io.grpc/grpc-protobuf "1.34.1"]
                 [io.grpc/grpc-stub "1.34.1"]
                 [io.grpc/grpc-netty "1.34.1"]
                 [io.netty/netty-tcnative-boringssl-static "2.0.31.Final"]
                 [javax.annotation/javax.annotation-api "1.3.2"]]

  :source-paths ["src"]
  :test-paths ["test"]
  :main products.main
  :uberjar-name "products.jar"
  
  :profiles {:uberjar {:aot :all}
             :kaocha {:dependencies [[lambdaisland/kaocha "0.0-554"]
                                     [lambdaisland/kaocha-midje "0.0-5"]
                                     [midje "1.9.9"]]}}
  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]}

  :plugins [[lein-cljfmt "0.7.0"]
            [lein-auto "0.1.3"]]

  :java-source-paths ["gen"])
