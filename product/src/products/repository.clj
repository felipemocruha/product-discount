(ns products.repository
  (:require [next.jdbc.sql :as sql]
            [next.jdbc :as jdbc])
  (:gen-class))

(defn list-products [db last-result]
  (with-open [conn (jdbc/get-connection db)]
    (sql/query conn ["SELECT * FROM product WHERE sequence > ? LIMIT 4"
                     last-result])))
