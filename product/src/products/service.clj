(ns products.service
  (:require [io.pedestal.http :as server]
            [reitit.pedestal :as pedestal]
            [reitit.http :as http]
            [reitit.ring :as ring]
            [jsonista.core :as json]
            [clojure.edn :as edn]
            [taoensso.timbre :as log]
            [products.core :as core]
            [products.discount :as discount])
  (:gen-class))

(defn- make-response [status body]
  {:status status
   :headers {"Content-Type" "application/json"}
   :body (json/write-value-as-bytes body)})

(defprotocol IService
  (list-products [this ctx] "Lists products optionally with discount value")
  (start [this] "Starts listening for requests"))

(defn req->user-id [req]
  (get-in req [:headers "x-user-id"]))

(defn req->page [req]
  (get-in req [:query-params :last_result]))

(defn err->code [err]
  (err {:not-found 404
        :invalid 400
        :internal 500}))

(defrecord Service [config]
  IService

  (list-products [_ req]
    (let [ctx {:user-id (req->user-id req)
               :last-result (Integer. (or (req->page req) "0"))
               :client (discount/client config)
               :db (:db config)}
          result (core/list-products ctx)]
      (if (nil? (:error result))
        (make-response 200 result)
        (make-response (err->code (:error result)) (dissoc result :error)))))

  (start [this]
    (let [routes
          [["/products" {:handler (partial list-products this)}]
           ["/healthcheck" {:handler
                            (fn [_] (make-response 200 {:healthy true}))}]]]

      (-> {::server/type :jetty
           ::server/port (get-in config [:server :port])
           ::server/join? false
           ::server/routes []}

          (server/default-interceptors)
          (pedestal/replace-last-interceptor
           (pedestal/routing-interceptor
            (http/router routes)))
          (server/create-server)
          (server/start)))))

(defn load-config []
  (try
    (edn/read-string (slurp "config.edn"))
    (catch Exception e
      (log/fatal "failed to load config file: " (.getMessage e))
      (System/exit 1))))

(defn create-service [config]
  (->Service config))
