(ns products.core
  (:require [taoensso.timbre :as log]
            [products.repository :as repo]
            [products.discount :as discount])
  (:gen-class))

(defn format-result [product discount]
  {:id (str (:product/id product))
   :price_in_cents (:product/price_in_cents product)
   :title (:product/title product)
   :description (:product/description product)
   :discount discount})

(defn process-product [client user-id product]
  (->> (discount/calculate-discount client user-id product)
       (format-result product)))

(defn list-products [{:keys [db last-result client user-id]}]
  (try
    (let [products (repo/list-products db last-result)
          last-result (:product/sequence (last products))
          result (pmap (partial process-product client user-id) products)]
      {:products result
       :meta {:last_result last-result}})
    (catch Exception e
      (log/error "failed to list products: " (.getMessage e))
      {:error :internal :message "Internal Error"})))
