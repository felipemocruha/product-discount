(ns products.main
  (:require [products.service :as service]
            [taoensso.timbre :as timbre]
            [products.log :as log]
            [products.service :as svc])
  (:gen-class))

(defn -main [& args]
  (timbre/set-level! :info)
  ;(timbre/merge-config! {:output-fn log/log-formatter})

  (-> (svc/load-config)
      svc/create-service
      svc/start))
