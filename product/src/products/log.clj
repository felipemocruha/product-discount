(ns products.log
  (:require [taoensso.timbre :as log]
            [jsonista.core :as json])
  (:gen-class))

(defn log-formatter [{:keys [level instant msg_
                             ?file ?line]}]
  (json/write-value-as-string
   {:level level
    :timestamp instant
    :message (force msg_)
    :file ?file
    :line ?line}))
