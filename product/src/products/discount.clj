(ns products.discount
  (:require [clojure.java.io :as io]
            [taoensso.timbre :as log])
  (:import [io.gitlab.felipemocruha.discount.v1
            DiscountAPIGrpc
            Discount
            Discount$CalculateDiscountRequest]
           [io.netty.channel ChannelOption]
           [io.grpc ManagedChannelBuilder]
           [io.grpc.netty GrpcSslContexts]
           [io.grpc.netty NettyChannelBuilder])
  (:gen-class))

(defn tls-stub [config]
  (let [host (:host config)
        port (:port config)
        public-key (io/resource (:cert-path config))
        cert-chain (io/resource (:chain-path config))
        private-key (io/resource (:key-path config))
        ctx (-> (GrpcSslContexts/forClient)
                (.trustManager public-key)
                (.keyManager cert-chain private-key)
                (.build))]
    (DiscountAPIGrpc/newBlockingStub
     (-> (NettyChannelBuilder/forAddress host port)
         (.withOption ChannelOption/CONNECT_TIMEOUT_MILLIS (int (:timeout config)))
         (.overrideAuthority "localhost")
         (.sslContext ctx)
         (.build)))))

(defn insecure-stub [config]
  (let [host (:host config)
        port (:port config)]
    (DiscountAPIGrpc/newBlockingStub
     (-> (NettyChannelBuilder/forAddress host port)
         (.withOption ChannelOption/CONNECT_TIMEOUT_MILLIS (int (:timeout config)))
         (.usePlaintext)
         (.build)))))

(defprotocol IDiscountClient
  (calculate-discount [this user-id product] "Calculate discount for a given product/user pair"))

(defn discount-resp->map [resp]
  {:percentage (.getPercentage resp)
   :value_in_cents (.getValueInCents resp)})

(defrecord DiscountClient [stub]
  IDiscountClient

  (calculate-discount [_ user-id product]
    (try
      (let [req (-> (Discount$CalculateDiscountRequest/newBuilder)
                    (.setProductId (str (:product/id product)))
                    (.setUserId user-id)
                    (.build))]
        (-> (.calculateDiscount stub req)
            (discount-resp->map)))
      (catch Exception e
        (log/error "failed to call discount service: " (str e))
        {:percentage 0.0
         :value_in_cents (:product/price_in_cents product)}))))

(defn client [config]
  (->DiscountClient (insecure-stub (:discount config))))
