CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Create tables

CREATE TABLE IF NOT EXISTS product(
       id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
       sequence SERIAL UNIQUE,
       price_in_cents DECIMAL,
       title text,
       description text
);

CREATE TABLE IF NOT EXISTS customer(
       id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
       first_name text,
       last_name text,
       date_of_birth TIMESTAMP
);

CREATE INDEX IF NOT EXISTS sequence_index ON product USING btree (sequence);

-- Add some data

INSERT INTO product(price_in_cents, title, description)
VALUES
    (82900, 'Smartphone LG K22 Vermelho', 'Android 10 Quad-core 1.3Ghz 13MP 2GB Ram 32GB'),
    (6890, 'Kit Sabão Líquido Omo', 'Lavagem Perfeita - 5L - 2 Unidades'),
    (5990, 'Pilha AA Pequena Duracell', 'Alcalina 16 Unidades'),
    (1059, 'Ketchup Tradicional Heinz', '397g'),
    (290, 'Biscoito Doce Adria', 'Frutas Vermelhas Integral Plus Life'),
    (3599, 'Boneco Inflável João Bobo', 'Lutador Mascarado Boxeador Intex'),
    (174900, 'Sofá Retrátil e Reclinável', 'Rio De Janeiro Tecido Suede 2,10m Marrom - Aifos');

INSERT INTO customer(first_name, last_name, date_of_birth)
VALUES
    ('Arnaldo', 'Silveira', '1958-01-04T00:00:00Z'::timestamp),
    ('Lucas', 'Souza', '1994-04-22T00:00:00Z'::timestamp), 
    ('Fernanda', 'Garcia', '1937-07-09T00:00:00Z'::timestamp);
