FROM clojure:openjdk-11-lein as builder

COPY . /products
WORKDIR /products
RUN lein uberjar


FROM gcr.io/distroless/java:11

COPY --from=builder /products/target/products.jar /

ENTRYPOINT ["java", "-cp", "products.jar", "clojure.main", "-m", "products.main"]
