FROM golang:1.15 as builder

COPY . /build
WORKDIR /build
RUN make compile

FROM gcr.io/distroless/static

COPY --from=builder /build/discount /

ENTRYPOINT ["/discount"]
