package main

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/felipemocruha/discount/gen/discount/v1"
)

func (s *Service) CalculateDiscount(ctx context.Context, req *v1.CalculateDiscountRequest) (*v1.CalculateDiscountResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, s.timeout*time.Second)
	defer cancel()

	discount, err := s.core.CalculateDiscount(req.ProductId, req.UserId, s.getTime())
	if err == nil {
		percentage, _ := discount.Percentage.Float64()
		return &v1.CalculateDiscountResponse{
			Percentage:   percentage,
			ValueInCents: discount.ValueInCents.IntPart(),
		}, nil
	}

	if _, ok := err.(NotFoundError); ok {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	log.Error().Msgf("failed to calculate discount: %v", err.Error())
	return nil, status.Error(codes.Internal, "Internal Error")
}
