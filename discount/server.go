package main

import (
	"net"

	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"

	v1 "gitlab.com/felipemocruha/discount/gen/discount/v1"
)

type Server interface {
	Start(*Service)
	Close()
}

type GrpcServer struct {
	host string
	grpc *grpc.Server
}

func (s *GrpcServer) Start(svc *Service) {
	log.Info().Msgf("starting server at: %v", s.host)

	listen, err := net.Listen("tcp", s.host)
	if err != nil {
		log.Fatal().Msgf("failed to launch server: %v", err)
	}
	v1.RegisterDiscountAPIServer(s.grpc, svc)

	s.grpc.Serve(listen)
}

func (s *GrpcServer) Close() {
	s.grpc.GracefulStop()
}

func NewServer(config *Config) Server {
	if config.Server.Insecure {
		return insecureServer(config)
	}

	return tlsServer(config)
}

func tlsServer(config *Config) Server {
	creds, err := credentials.NewServerTLSFromFile(
		config.Server.CertPath,
		config.Server.KeyPath,
	)
	if err != nil {
		log.Fatal().Msgf("failed to load certificates: %v", err)
	}

	server := grpc.NewServer(
		grpc.Creds(creds),
		grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor),
	)
	if config.Server.Reflection {
		reflection.Register(server)
	}

	return &GrpcServer{
		config.Server.Host,
		server,
	}
}

func insecureServer(config *Config) Server {
	return &GrpcServer{
		config.Server.Host,
		grpc.NewServer(
			grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor),
		),
	}
}
