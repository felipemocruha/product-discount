package main

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v1 "gitlab.com/felipemocruha/discount/gen/discount/v1"
)

func init() {
	zerolog.SetGlobalLevel(zerolog.Disabled)
}

func TestServiceCalculateDiscountOk(t *testing.T) {
	birth, _ := time.Parse(time.RFC3339, "1998-11-27T10:23:05Z")
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"

	core := &Core{
		config: &Config{
			Server: ServerConfig{
				Timeout: 1,
			},
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			product: &Product{
				productId,
				decimal.NewFromInt(19999),
			},
			user: &User{
				userId,
				birth,
			},
		},
	}

	service := &Service{
		core:    core,
		getTime: func() time.Time { return today },
	}

	req := &v1.CalculateDiscountRequest{
		ProductId: productId,
		UserId:    userId,
	}

	resp, err := service.CalculateDiscount(context.Background(), req)
	assert.Nil(t, err)
	assert.Equal(t, float64(10.0), resp.Percentage)
	assert.Equal(t, int64(17999), resp.ValueInCents)
}

func TestServiceCalculateDiscountProductNotFoundError(t *testing.T) {
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			productErr: NotFoundError{"product", "1"},
		},
	}

	service := &Service{
		core:    core,
		getTime: func() time.Time { return today },
	}

	req := &v1.CalculateDiscountRequest{
		ProductId: productId,
		UserId:    userId,
	}
	_, err := service.CalculateDiscount(context.Background(), req)
	st, _ := status.FromError(err)
	assert.Equal(t, st.Code(), codes.NotFound)
}

func TestServiceCalculateDiscountFetchProductError(t *testing.T) {
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			productErr: errors.New("ops"),
		},
	}

	service := &Service{
		core:    core,
		getTime: func() time.Time { return today },
	}

	req := &v1.CalculateDiscountRequest{
		ProductId: productId,
		UserId:    userId,
	}
	_, err := service.CalculateDiscount(context.Background(), req)
	st, _ := status.FromError(err)
	assert.Equal(t, st.Code(), codes.Internal)
}

func TestServiceCalculateDiscountFetchUserError(t *testing.T) {
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			userErr: errors.New("ops"),
		},
	}

	service := &Service{
		core:    core,
		getTime: func() time.Time { return today },
	}

	req := &v1.CalculateDiscountRequest{
		ProductId: productId,
		UserId:    userId,
	}
	_, err := service.CalculateDiscount(context.Background(), req)
	st, _ := status.FromError(err)
	assert.Equal(t, st.Code(), codes.Internal)
}

func TestServiceCalculateDiscountUserNotFoundError(t *testing.T) {
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			userErr: NotFoundError{"user", "1"},
		},
	}

	service := &Service{
		core:    core,
		getTime: func() time.Time { return today },
	}

	req := &v1.CalculateDiscountRequest{
		ProductId: productId,
		UserId:    userId,
	}
	_, err := service.CalculateDiscount(context.Background(), req)
	st, _ := status.FromError(err)
	assert.Equal(t, st.Code(), codes.NotFound)
}
