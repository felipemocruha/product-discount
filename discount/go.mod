module gitlab.com/felipemocruha/discount

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.9.0
	github.com/prometheus/client_golang v1.8.0 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.5.1
	gitlab.com/felipemocruha/gonfig v0.2.0
	google.golang.org/grpc v1.34.0
)
