package main

import (
	"database/sql"
	"errors"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

type StubRepository struct {
	product    *Product
	productErr error

	user    *User
	userErr error
}

func (r StubRepository) GetUser(id string) (*User, error) {
	return r.user, r.userErr
}

func (r StubRepository) GetProduct(id string) (*Product, error) {
	return r.product, r.productErr
}

func (r StubRepository) Close() {}

func TestMakeConnStr(t *testing.T) {
	config := &Config{
		Db: DbConfig{
			Host:     "0.0.0.0",
			User:     "test",
			Name:     "test",
			Password: "test",
			Port:     "5432",
		},
	}

	expected := "host=0.0.0.0 user=test dbname=test sslmode=disable password=test port=5432"
	assert.Equal(t, expected, makeConnStr(config))
}

func TestGetUserOk(t *testing.T) {
	userId := "ffb904c7-e6ae-4b8a-8d60-5fa31b034503"
	birth, _ := time.Parse(time.RFC3339, "1998-11-27T10:23:05Z")
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	defer db.Close()

	cols := []string{"id", "date_of_birth"}
	mock.ExpectQuery("SELECT id, date_of_birth FROM customer WHERE id = (.+)").
		WithArgs(userId).
		WillReturnRows(sqlmock.NewRows(cols).AddRow(userId, birth))

	repo := Postgres{sqlx.NewDb(db, "sqlmock")}
	user, err := repo.GetUser(userId)

	assert.Nil(t, err)
	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, userId, user.Id)
	assert.True(t, birth.Equal(user.DateOfBirth))
}

func TestGetUserNotFound(t *testing.T) {
	userId := "ffb904c7-e6ae-4b8a-8d60-5fa31b034503"
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	defer db.Close()

	mock.ExpectQuery("SELECT id, date_of_birth FROM customer WHERE id = (.+)").
		WillReturnError(sql.ErrNoRows)

	repo := Postgres{sqlx.NewDb(db, "sqlmock")}
	_, err = repo.GetUser(userId)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.IsType(t, NotFoundError{}, err)
}

func TestGetUserInternalError(t *testing.T) {
	userId := "ffb904c7-e6ae-4b8a-8d60-5fa31b034503"
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	defer db.Close()

	mock.ExpectQuery("SELECT id, date_of_birth FROM customer WHERE id = (.+)").
		WillReturnError(errors.New("oh noooo!"))

	repo := Postgres{sqlx.NewDb(db, "sqlmock")}
	_, err = repo.GetUser(userId)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.IsType(t, InternalError{}, err)
}

func TestGetProductOk(t *testing.T) {
	productId := "ffb904c7-e6ae-4b8a-8d60-5fa31b034503"
	priceInCents := decimal.NewFromInt(2599)
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	defer db.Close()

	cols := []string{"id", "price_in_cents"}
	mock.ExpectQuery("SELECT id, price_in_cents FROM product WHERE id = (.+)").
		WithArgs(productId).
		WillReturnRows(sqlmock.NewRows(cols).AddRow(productId, priceInCents))

	repo := Postgres{sqlx.NewDb(db, "sqlmock")}
	product, err := repo.GetProduct(productId)

	assert.Nil(t, err)
	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, productId, product.Id)
	assert.True(t, priceInCents.Equal(product.PriceInCents))
}

func TestGetProductNotFound(t *testing.T) {
	productId := "ffb904c7-e6ae-4b8a-8d60-5fa31b034503"
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	defer db.Close()

	mock.ExpectQuery("SELECT id, price_in_cents FROM product WHERE id = (.+)").
		WillReturnError(sql.ErrNoRows)

	repo := Postgres{sqlx.NewDb(db, "sqlmock")}
	_, err = repo.GetProduct(productId)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.IsType(t, NotFoundError{}, err)
}

func TestGetProductInternalError(t *testing.T) {
	productId := "ffb904c7-e6ae-4b8a-8d60-5fa31b034503"
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	defer db.Close()

	mock.ExpectQuery("SELECT id, price_in_cents FROM product WHERE id = (.+)").
		WillReturnError(errors.New("oh noooo!"))

	repo := Postgres{sqlx.NewDb(db, "sqlmock")}
	_, err = repo.GetProduct(productId)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.IsType(t, InternalError{}, err)
}
