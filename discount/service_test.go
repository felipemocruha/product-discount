package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type SpyServer struct {
	startCalled bool
	closeCalled bool
	startedChan chan struct{}
}

func (s *SpyServer) Start(svc *Service) {
	s.startCalled = true
	s.startedChan <- struct{}{}
}

func (s *SpyServer) Close() {
	s.closeCalled = true
}

func TestServiceStart(t *testing.T) {
	startedChan := make(chan struct{}, 1)
	config := &Config{}
	repo := StubRepository{}
	core := NewCore(config, repo)
	server := &SpyServer{startedChan: startedChan}

	shutdown := make(chan struct{})
	NewService(config, core, server).Start(shutdown)
	<-startedChan

	assert.True(t, server.startCalled)
}

func TestServiceCloseWithSigterm(t *testing.T) {
	startedChan := make(chan struct{})
	config := &Config{}
	repo := StubRepository{}
	core := NewCore(config, repo)
	server := &SpyServer{startedChan: startedChan}

	go func() {
		<-startedChan

		proc, err := os.FindProcess(os.Getpid())
		assert.Nil(t, err)

		proc.Signal(os.Interrupt)
	}()

	shutdown := make(chan struct{})
	NewService(config, core, server).Start(shutdown)

	<-shutdown
	assert.True(t, server.closeCalled)
}
