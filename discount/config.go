package main

type ServerConfig struct {
	Host       string `yaml:"host"`
	CertPath   string `yaml:"cert-path"`
	KeyPath    string `yaml:"key-path"`
	Timeout    int    `yaml:"timeout"`
	Reflection bool   `yaml:"reflection"`
	Insecure   bool   `yaml:"insecure"`
}

type DbConfig struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Name     string `yaml:"name"`
	Port     string `yaml:"port"`
}

type BusinessConfig struct {
	UserBirthdayDiscount  float64 `yaml:"user-birthday-discount"`
	BlackFridayDiscount   float64 `yaml:"black-friday-discount"`
	MaxDiscountPercentage float64 `yaml:"max-discount-percentage"`
	BlackFridayDate       string  `yaml:"black-friday-date"`
}

type Config struct {
	Server   ServerConfig   `yaml:"server"`
	Db       DbConfig       `yaml:"db"`
	Business BusinessConfig `yaml:"business"`
}
