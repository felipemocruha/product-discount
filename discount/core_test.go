package main

import (
	"errors"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

func TestDayAndMonth(t *testing.T) {
	date, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
	assert.Equal(t, "2/1", dayAndMonth(date))
}

func TestApplyDiscountNoDiscountConditionsMet(t *testing.T) {
	birth, _ := time.Parse(time.RFC3339, "1998-11-28T15:04:05Z")

	ctx := discountCtx{
		product: &Product{
			"9e9ef34d-9b88-4e81-afd7-4da2ed3020d2",
			decimal.NewFromInt(19999),
		},
		user: &User{
			"aca153bc-b777-4e32-b6ca-e0c98148fc27",
			birth,
		},
	}

	today, _ := time.Parse(time.RFC3339, "2020-11-25T15:04:05Z")
	ctx.today = today
	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
			},
		},
	}

	discount, price := core.applyDiscount(ctx)
	assert.True(t, decimal.NewFromFloat(0.0).Equal(discount))
	assert.True(t, decimal.NewFromInt(19999).Equal(price))
}

func TestApplyDiscountBirthdayOnly(t *testing.T) {
	birth, _ := time.Parse(time.RFC3339, "1998-11-28T15:04:05Z")

	ctx := discountCtx{
		product: &Product{
			"9e9ef34d-9b88-4e81-afd7-4da2ed3020d2",
			decimal.NewFromInt(19999),
		},
		user: &User{
			"aca153bc-b777-4e32-b6ca-e0c98148fc27",
			birth,
		},
	}

	today, _ := time.Parse(time.RFC3339, "2020-11-28T15:04:05Z")
	ctx.today = today
	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
	}

	discount, price := core.applyDiscount(ctx)
	assert.True(t, decimal.NewFromFloat(5.0).Equal(discount))
	assert.True(t, decimal.NewFromInt(18999).Equal(price))
}

func TestApplyDiscountBlackFridayOnly(t *testing.T) {
	birth, _ := time.Parse(time.RFC3339, "1998-11-28T15:04:05Z")

	ctx := discountCtx{
		product: &Product{
			"9e9ef34d-9b88-4e81-afd7-4da2ed3020d2",
			decimal.NewFromInt(19999),
		},
		user: &User{
			"aca153bc-b777-4e32-b6ca-e0c98148fc27",
			birth,
		},
	}

	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")
	ctx.today = today
	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
	}

	discount, price := core.applyDiscount(ctx)
	assert.True(t, decimal.NewFromFloat(10.0).Equal(discount))
	assert.True(t, decimal.NewFromInt(17999).Equal(price))
}

func TestApplyDiscountBlackFridayAndBirthday(t *testing.T) {
	birth, _ := time.Parse(time.RFC3339, "1998-11-27T10:23:05Z")

	ctx := discountCtx{
		product: &Product{
			"9e9ef34d-9b88-4e81-afd7-4da2ed3020d2",
			decimal.NewFromInt(19999),
		},
		user: &User{
			"aca153bc-b777-4e32-b6ca-e0c98148fc27",
			birth,
		},
	}

	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")
	ctx.today = today
	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
	}

	discount, price := core.applyDiscount(ctx)
	assert.True(t, decimal.NewFromFloat(10.0).Equal(discount))
	assert.True(t, decimal.NewFromInt(17999).Equal(price))
}

func TestCalculateDiscountOk(t *testing.T) {
	birth, _ := time.Parse(time.RFC3339, "1998-11-27T10:23:05Z")
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			product: &Product{
				productId,
				decimal.NewFromInt(19999),
			},
			user: &User{
				userId,
				birth,
			},
		},
	}

	discount, err := core.CalculateDiscount(productId, userId, today)
	assert.Nil(t, err)
	assert.True(t, decimal.NewFromFloat(10.0).Equal(discount.Percentage))
	assert.True(t, decimal.NewFromInt(17999).Equal(discount.ValueInCents))
}

func TestCalculateDiscountFetchProductError(t *testing.T) {
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			productErr: errors.New("ooops"),
		},
	}

	_, err := core.CalculateDiscount(productId, userId, today)
	assert.Equal(t, errors.New("ooops"), err)
}

func TestCalculateDiscountFetchUserError(t *testing.T) {
	productId := "9e9ef34d-9b88-4e81-afd7-4da2ed3020d2"
	userId := "aca153bc-b777-4e32-b6ca-e0c98148fc27"
	today, _ := time.Parse(time.RFC3339, "2020-11-27T15:04:05Z")

	core := &Core{
		config: &Config{
			Business: BusinessConfig{
				UserBirthdayDiscount:  5,
				BlackFridayDiscount:   10,
				MaxDiscountPercentage: 10,
				BlackFridayDate:       "27/11",
			},
		},
		repository: StubRepository{
			product: &Product{
				productId,
				decimal.NewFromInt(19999),
			},
			userErr: errors.New("ooops"),
		},
	}

	_, err := core.CalculateDiscount(productId, userId, today)
	assert.Equal(t, errors.New("ooops"), err)
}
