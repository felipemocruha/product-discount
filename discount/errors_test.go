package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotFoundErrorMsg(t *testing.T) {
	err := NotFoundError{"product", "8adadb41-6fdc-402c-9755-ff1028c6d7ae"}

	assert.Equal(t, "product with id 8adadb41-6fdc-402c-9755-ff1028c6d7ae not found", err.Error())
}

func TestInternalErrorMsg(t *testing.T) {
	err := InternalError{errors.New("test")}

	assert.Equal(t, "internal error: test", err.Error())
}
