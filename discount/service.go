package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Service struct {
	server  Server
	core    *Core
	getTime func() time.Time
	timeout time.Duration
}

func NewService(config *Config, core *Core, server Server) *Service {
	svc := &Service{
		server:  server,
		core:    core,
		getTime: time.Now,
		timeout: time.Duration(config.Server.Timeout),
	}

	return svc
}

func (s *Service) Start(shutdown chan struct{}) {
	go s.CloseWithSigterm(shutdown)
	s.server.Start(s)
}

func (s *Service) CloseWithSigterm(shutdown chan struct{}) {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, syscall.SIGTERM)
	<-sigc

	s.server.Close()
	s.core.Close()
	close(shutdown)
}
