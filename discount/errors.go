package main

import (
	"fmt"
)

type NotFoundError struct {
	ctx string
	id  string
}

func (e NotFoundError) Error() string {
	return fmt.Sprintf("%v with id %v not found", e.ctx, e.id)
}

type InternalError struct {
	err error
}

func (e InternalError) Error() string {
	return fmt.Sprintf("internal error: %v", e.err.Error())
}
