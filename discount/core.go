package main

import (
	"fmt"
	"time"

	"github.com/shopspring/decimal"
)

type Core struct {
	repository Repository
	config     *Config
}

type User struct {
	Id          string    `db:"id"`
	DateOfBirth time.Time `db:"date_of_birth"`
}

type Product struct {
	Id           string          `db:"id"`
	PriceInCents decimal.Decimal `db:"price_in_cents"`
}

type Discount struct {
	Percentage   decimal.Decimal
	ValueInCents decimal.Decimal
}

func NewCore(config *Config, repository Repository) *Core {
	return &Core{
		repository: repository,
		config:     config,
	}
}

func (c *Core) Close() {
	c.repository.Close()
}

type discountCtx struct {
	product *Product
	user    *User
	today   time.Time
}

type shouldApplyDiscount func(ctx discountCtx) bool

func dayAndMonth(t time.Time) string {
	return fmt.Sprintf("%v/%v", t.Day(), int(t.Month()))
}

var (
	zero       = decimal.NewFromFloat(0.0)
	oneHundred = decimal.NewFromFloat(100.0)
)

func (c *Core) applyDiscount(ctx discountCtx) (decimal.Decimal, decimal.Decimal) {
	values := map[string]decimal.Decimal{
		"user_birthday": decimal.NewFromFloat(c.config.Business.UserBirthdayDiscount),
		"black_friday":  decimal.NewFromFloat(c.config.Business.BlackFridayDiscount),
	}

	rules := map[string]shouldApplyDiscount{
		"user_birthday": func(ctx discountCtx) bool {
			return dayAndMonth(ctx.today) == dayAndMonth(ctx.user.DateOfBirth)
		},
		"black_friday": func(ctx discountCtx) bool {
			return dayAndMonth(ctx.today) == c.config.Business.BlackFridayDate
		},
	}

	discount := zero
	for rule, val := range values {
		if apply := rules[rule](ctx); apply {
			discount = discount.Add(val)
		}
	}

	maxDiscount := decimal.NewFromFloat(c.config.Business.MaxDiscountPercentage)
	if discount.GreaterThan(maxDiscount) {
		discount = maxDiscount

	} else if discount.Equal(zero) {
		return discount, ctx.product.PriceInCents
	}

	// Floor is used to be fair with the customers by not giving less discount than promised
	value := ctx.product.PriceInCents.
		Mul(oneHundred.Sub(discount).Div(oneHundred)).
		Floor()

	return discount, value
}

func (c *Core) CalculateDiscount(productId, userId string, today time.Time) (*Discount, error) {
	type Result struct {
		kind    string
		err     error
		product *Product
		user    *User
	}
	results := make(chan Result)

	go func() {
		product, err := c.repository.GetProduct(productId)
		results <- Result{kind: "product", err: err, product: product}
	}()

	go func() {
		user, err := c.repository.GetUser(userId)
		results <- Result{kind: "user", err: err, user: user}
	}()

	var product *Product
	var user *User

	for completed := 0; completed < 2; {
		result := <-results
		if result.err != nil {
			return nil, result.err

		} else if result.kind == "product" {
			product = result.product
			completed++

		} else {
			user = result.user
			completed++
		}
	}

	percentage, discount := c.applyDiscount(discountCtx{product, user, today})
	return &Discount{percentage, discount}, nil
}
