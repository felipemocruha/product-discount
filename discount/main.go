// +build !test

package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/felipemocruha/gonfig"
)

func main() {
	log.Logger = log.With().Caller().Logger()

	config := &Config{}
	err := gonfig.LoadFromEnv("CONFIG_PATH", config, "config.yaml")
	if err != nil {
		log.Fatal().Msgf("failed to load config: %v", err)
	}

	repo := NewRepository(config)
	core := NewCore(config, repo)
	server := NewServer(config)

	shutdown := make(chan struct{})
	go NewService(config, core, server).Start(shutdown)

	<-shutdown
	log.Info().Msg("graceful shutdown completed")
}
