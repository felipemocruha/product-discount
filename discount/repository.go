package main

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog/log"
)

type Repository interface {
	GetUser(id string) (*User, error)
	GetProduct(id string) (*Product, error)
	Close()
}

type Postgres struct {
	conn *sqlx.DB
}

func NewRepository(config *Config) Repository {
	conn, err := sqlx.Connect("postgres", makeConnStr(config))
	if err != nil {
		log.Fatal().Msgf("failed to create database connection: %v", err)
	}

	return &Postgres{conn}
}

const (
	GET_PRODUCT = "SELECT id, price_in_cents FROM product WHERE id = $1"
	GET_USER    = "SELECT id, date_of_birth FROM customer WHERE id = $1"
)

func (pg *Postgres) GetUser(id string) (*User, error) {
	user := &User{}
	err := pg.conn.Get(user, GET_USER, id)

	switch err {
	case nil:
		return user, nil

	case sql.ErrNoRows:
		return nil, NotFoundError{"user", id}

	default:
		return nil, InternalError{err}
	}
}

func (pg *Postgres) GetProduct(id string) (*Product, error) {
	product := &Product{}
	err := pg.conn.Get(product, GET_PRODUCT, id)

	switch err {
	case nil:
		return product, nil

	case sql.ErrNoRows:
		return nil, NotFoundError{"product", id}

	default:
		return nil, InternalError{err}
	}
}

func (pg *Postgres) Close() {
	pg.conn.Close()
}

func makeConnStr(config *Config) string {
	return fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s port=%s",
		config.Db.Host,
		config.Db.User,
		config.Db.Name,
		config.Db.Password,
		config.Db.Port,
	)
}
