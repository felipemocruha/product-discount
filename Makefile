
generate:
	@cd proto && prototool generate && cd ..
	@cd product && gradle build

postgres:
	@docker run -d -p 5433:5432 --name productdiscountdb -e POSTGRES_USER=productdiscount -e POSTGRES_PASSWORD=productdiscount -e POSTGRES_DB=productdiscount -e PGDATA=/var/lib/postgresql/data/pgdata postgres:12

db: postgres
	@sleep 3
	@docker exec -i productdiscountdb psql -U productdiscount -d productdiscount < schema.sql

build:
	@cd discount && make build_docker && cd ..
	@cd product && make build_docker

run:
	@cd discount && make run_docker && cd ..
	@cd product && make run_docker

clean:
	@rm -rf gen
