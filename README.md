# product-discount

## Overview

This repository is composed by 2 services:
 - **discount**: a gRPC service responsible for calculating product discounts based on business rules
- **product**: a frontend JSON+REST service responsible for producing product data with discounts consumed by a webpage

### Architecture

![architecture](diagram.png)

### Organization

- proto: protocol buffers used by the services
- discount: discount service source and config
- product: product service source and config
- dockerfiles: services dockerfiles
- credentials: place TLS certificates here when needed

## Running

To run you need `docker` and `make` installed.

**Make sure you have the following ports available to listen locally: 5433, 9998, 9999**

On the first run, begin by starting the database:

```
make db
```

This will run a PostgreSQL container and create initial schema and sample data.

then:

```
make run
```

This will start both services and we're good to go!

## Configurations

### discount

```yaml
server:
  host: localhost:9999 # address the server must bind
  cert-path: "../credentials/localhost.crt" # optional when insecure == true
  key-path: "../credentials/localhost.key" # optional when insecure == true
  timeout: 1 # server response timeout
  reflection: true # enable server reflection, useful with grpcurl
  insecure: true # run without TLS
  
db:
  host: 0.0.0.0 # database address
  user: productdiscount # database user
  password: productdiscount # database password
  name: productdiscount # database name
  port: 5433 # database port

business:
  user-birthday-discount: 5 # user birthday discount value
  black-friday-discount: 10 # black friday discount value
  max-discount-percentage: 10 # max discount that must be allowed
  black-friday-date: 25/11 # when black friday is (e.g. 4/1, 10/11...)
```

### product

```edn
{:db {:dbtype "postgresql"
      :dbname "productdiscount" ;; database name
      :host "0.0.0.0" ;; database address
      :user "productdiscount" ;; database user
      :password "productdiscount" ;; database password
      :port 5433} ;;database port

 :server {:port 9998} ;; address the server must bind

 :discount {:host "localhost" ;; discount service address
            :port 9999 ;; discount service listen port
            :cert-path "../credentials/localhost.crt" ;; optional when using insecure
            :chain-path "../credentials/localhost.crl" ;; optional when using insecure
            :key-path "../credentials/localhost.key"}} ;; optional when using insecure
```

## Design Decisions

Languages used:
 - Go (discount)
 - Clojure (product)
 
Database: PostgreSQL

### Pagination

The product service `/products` endpoint supports pagination by query parameters, the next page token is returned under the `meta.last_result` object on response body.

example response body:

```
{
  "products": [
    {
      "id": "9f258b91-fe21-483a-8222-e9d69be421cd",
      "price_in_cents": 82900,
      "title": "Smartphone LG K22 Vermelho",
      "description": "Android 10 Quad-core 1.3Ghz 13MP 2GB Ram 32GB",
      "discount": {
        "percentage": 5,
        "value_in_cents": 78755
      }
    },
  ],
  "meta": {
    "last_result": 4
  }
}
```

example next page request:

```
http://localhost:9998/products?last_result=4
```

When there are no more products to return, an empty list of products is returned.

### User Identity

The product service supports a header `X-User-Id` that is the user's id on the database, this is used by the discount service to process business rules. When not provided, the user will not get any discount.

### Concurrency

On product service, after fetching the list of products the discount service is called in parallel for each result.

On discount service, the product info and user info are fetched concurrently.

## Considerations

- Since the objective of this challenge is to showcase development and distributed systems techniques, both services share the same database for simplicity, in a production system this would not be ideal.

- There is not authentication/authorization implemented, in a production system I would use some identity provider with OAuth2 + JWT.

- In a production system under high traffic, It could be a good performance boost to add a caching strategy between the product and discount services.
